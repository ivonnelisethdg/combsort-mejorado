/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CombSortMej;

/**
 *
 * @author SISTEMAS
 */
public class combSortMejorado {

    static int arr[];
 
    private static void combSortMejorado(int n){
        int sep = n;
        int copia = sep;
        int exponente = 0;
        while(copia != 1){
 
            copia = copia/2;
            exponente++;
        }
        sep = 1;
        for(int i = 0; i <= exponente; i++){
            sep *= 2;
        }
        while(sep != 1){
        	sep = sep/2;
            for(int i=0; i+sep<n; i++){
                int y = arr[i];                                   
                if(arr[i] > arr[i+sep]){
                    arr[i] = arr[i+sep];
                    arr[i+sep] = y;
                }
            }
        }
    }
    
    static void combSort(int n){
        int sep = n;                         
        boolean swap = false;                                
        while(sep!=1 || swap){                                
            swap = false;                                        
            int newGap = (sep * 10) / 13;                      
            if(newGap < 1){newGap = 1;}                     
            sep = newGap;                                     
            for(int i=0; i+sep<n; i++){
                int y = arr[i];                                   
                int res = ((Comparable)y).compareTo(arr[i+sep]);     
                if(res>0){
                    arr[i] = arr[i+sep];                             
                    arr[sep+i] = y;
                    swap = true;
                }
            }
        }
    }
    
    static void llenarArray(int n){
        for(int i = n; i >= 1; i--){
                arr[n - i] = i;
        }
    }
    
    static void imprimirArray(int n){
        for(int i = 0; i < n; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
    
    //1 para combsort
    //otro para mejorado
    static long medirTiempo(int opcion, int n){
        long time_start, time_end;
        time_start = System.currentTimeMillis();
        if(opcion == 1)
            combSort(n);
        else
            combSortMejorado(n);
        time_end = System.currentTimeMillis();
        return time_end - time_start;
    }
    
    
    public static void main(String[] args) {
        //MODIFICAR TAMAÑO
        System.out.println("PARA LISTAS DESCENDENTES");
        int n = 10000;
        arr = new int[n];
        System.out.println("Array Original");
        llenarArray(n);
        imprimirArray(n);
        System.out.println("COMBSORT");
        long t1 = medirTiempo(1, n);
        System.out.println("TIEMPO: "+ t1 + "ms");
        imprimirArray(n);
        System.out.println("Array Original");
        llenarArray(n);
        imprimirArray(n);
        System.out.println("COMBSORT MEJORADO");
        long t2 = medirTiempo(2, n);
        System.out.println("TIEMPO: "+ t2 + "ms");
        imprimirArray(n);
        System.out.println("DIFERENCIA DE TIEMPO: ORGINAL - MEJORADO:" + "\n" + (t1-t2) + "ms");
        
    }
    
}
